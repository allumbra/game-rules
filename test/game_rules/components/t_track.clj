(ns game-rules.components.track
  (:use midje.sweet)
  )


(linearTrack :values ["a" "b" "c"] :markers {:black 1})

(fact "create a linear track"
      (linearTrack :values ["a" "b" "c"] :markers {:black 1}) => #game_rules.components.track.LinearTrack{:values ["a" "b" "c"], :markers {:black 1}}
      )

(fact "advance marker n - constrain to values"
  (->
    (linearTrack :values ["N" "S" "E"] :markers {})
    (add-marker "black" 1)
    (move-marker-n "black" 1)
   ) => #game_rules.components.track.LinearTrack{:values ["N" "S" "E"], :markers {:black 2}}
)

      ;; retreat marker n

(def ctrack (->
    (circular-track :values ["N" "S" "E" "W"] :markers {})
    (add-marker "black" 0)
             )
  )
ctrack
(fact "retreat marker on linear track.  Demonstrate wrap around."
  (move-marker-n ctrack "black" 1) =>
      #game_rules.components.track.CircularTrack{:values ["N" "S" "E" "W"], :markers {:black 1}}
  (move-marker-n ctrack "black" 4) =>
      #game_rules.components.track.CircularTrack{:values ["N" "S" "E" "W"], :markers {:black 0}}
  (move-marker-n ctrack "black" -1) =>
      #game_rules.components.track.CircularTrack{:values ["N" "S" "E" "W"], :markers {:black 3}}
      )
