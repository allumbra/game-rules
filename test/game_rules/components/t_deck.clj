(ns game-rules.components.deck
  (:use midje.sweet)
  )


;; todo move to tests
(def cards (into [] (range 1 5)))
(shuffle-deck cards)

(fact "cards should not be in ascending order"
      (not= (shuffle-deck cards) [1 2 3 4 5]) => true
      )

(deck cards)

(fact "draw cards"
      (draw cards [])  => [[1 2 3] [4]]
      (draw-n cards 2) => [[1 2] [4 3]]
      (draw-n cards 3)  => [[1] [4 3 2]]
      (draw-n cards 11) => [[] [4 3 2 1]]
      )

(fact "discard on top of the deck"
      (discard cards 12) => [1 2 3 4 12]
      )
