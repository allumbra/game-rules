(ns game-rules.t_helo
  (:use midje.sweet)
  (:require [game-rules.helo :as h]
            [game-rules.rules :as r]
            [game-rules.components.track :as t])
  )

(fact "Add points for player"
  (h/score-points-for-player {:players [{:score 1} {:score 2} {:score 3} {:score 4}]} 2 3)
    =>  {:players [{:score 1} {:score 2} {:score 6} {:score 4}]}
      )

(def helo-game
  (->
    (h/create-helo-game [
                         {:name "ben" :color "yellow" :agent (h/player-agent h/highest-card-strategy)}
                         {:name "chris" :color "red" :agent (h/player-agent h/highest-card-strategy)}
                         {:name "pattumma" :color "pink" :agent (h/player-agent h/highest-card-strategy)}
                         {:name "sky" :color "blue" :agent (h/player-agent h/highest-card-strategy)}])
    (h/setup)
   )
)
helo-game

h/rules

(def to-round (get-in h/rules [:states :start :transitions 0]))
to-round


(fact "transition state"
      (let [g1  (r/transition helo-game h/rules to-round)
            g2  (r/transition g1 h/rules (get-in h/rules [:states :round :transitions 0]))
            p0  (get-in g2 [:players 0])
            g3  (r/transition g2 h/rules (get-in h/rules [:states :player-turn :transitions 0]))
             ]
  (:state g2)  => "player-turn"  ;; did the state get advanced
  (count (:hand p0)) => 10  ;; are we executing transition command scripts
  (count (:trick g3)) => 1
 ))

(fact "deal cards should result in player hands"
  (let [game-after-deal (h/deal-cards helo-game)]
    (count (get-in game-after-deal [:players 0 :hand])) => 10
    (count (get-in game-after-deal [:players 1 :hand])) => 10
    (count (get-in game-after-deal [:players 2 :hand])) => 10
    (count (get-in game-after-deal [:players 3 :hand])) => 10
    )
  )

(fact "scoring the various options"
  (h/score {:helo-track (t/circular-track :values ["High" "High-Even" "Low-Even" "Low" "Low-Odd"
                                             "High-Odd"]
                                    :markers {:black 0})
          :trick [{:player 3 :card 1} {:player 2 :card 2} {:player 1 :card 3} {:player 0 :card 4}]
          :players [ {:score 0} {:score 0} {:score 0} {:score 0}]
          :on-turn 0
          }
    ) => {:helo-track #game_rules.components.track.CircularTrack{:values ["High" "High-Even" "Low-Even" "Low" "Low-Odd" "High-Odd"], :markers {:black 0}}, :trick [{:player 3, :card 1} {:player 2, :card 2} {:player 1, :card 3} {:player 0, :card 4}], :players [{:score 2} {:score 1} {:score 0} {:score -1}], :on-turn 3}
  (h/score {:helo-track (t/circular-track :values ["High" "High-Even" "Low-Even" "Low" "Low-Odd"
                                           "High-Odd"]
                                  :markers {:black 1})
        :trick [{:player 3 :card 1} {:player 2 :card 2} {:player 1 :card 3} {:player 0 :card 4}]
        :players [ {:score 0} {:score 0} {:score 0} {:score 0}]
        :on-turn 0
        }
       )
(h/score {:helo-track (t/circular-track :values ["High" "High-Even" "Low-Even" "Low" "Low-Odd"
                                           "High-Odd"]
                                  :markers {:black 2})
        :trick [{:player 3 :card 1} {:player 2 :card 2} {:player 1 :card 3} {:player 0 :card 4}]
        :players [ {:score 0} {:score 0} {:score 0} {:score 0}]
        :on-turn 0
        }
       ) => {:helo-track #game_rules.components.track.CircularTrack{:values ["High" "High-Even" "Low-Even" "Low" "Low-Odd" "High-Odd"], :markers {:black 2}}, :trick [{:player 3, :card 1} {:player 2, :card 2} {:player 1, :card 3} {:player 0, :card 4}], :players [{:score 1} {:score 0} {:score 2} {:score 0}], :on-turn 1}
(h/score {:helo-track (t/circular-track :values ["High" "High-Even" "Low-Even" "Low" "Low-Odd"
                                           "High-Odd"]
                                  :markers {:black 3})
        :trick [{:player 3 :card 1} {:player 2 :card 2} {:player 1 :card 3} {:player 0 :card 4}]
        :players [ {:score 0} {:score 0} {:score 0} {:score 0}]
        :on-turn 0
        }
       )
      => {:helo-track #game_rules.components.track.CircularTrack{:values ["High" "High-Even" "Low-Even" "Low" "Low-Odd" "High-Odd"], :markers {:black 3}}, :trick [{:player 3, :card 1} {:player 2, :card 2} {:player 1, :card 3} {:player 0, :card 4}], :players [{:score -1} {:score 0} {:score 1} {:score 2}], :on-turn 2}

      )

(fact "advance next player"
      (let [g     (h/create-helo-game [{:name "ben" :color "yellow"}{:name "chris" :color "red"}{:name "pattumma" :color "pink"}{:name "sky" :color "blue"}])
          ]
  (h/advance-to-next-player g)
      => {:deck [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40], :helo-track #game_rules.components.track.CircularTrack{:values ["High" "High-Even" "Low-Even" "Low" "Low-Odd" "High-Odd"], :markers {:black 0}}, :player-area {:hand [], :played-card nil, :score 0}, :players [{:color "yellow", :name "ben", :hand [], :played-card nil, :score 0} {:color "red", :name "chris", :hand [], :played-card nil, :score 0} {:color "pink", :name "pattumma", :hand [], :played-card nil, :score 0} {:color "blue", :name "sky", :hand [], :played-card nil, :score 0}], :state "start", :on-turn 1, :trick []}
      )
)

(fact "adjust helo track to the next space if even/odd not tied"
  (h/adjust-helo-track {:trick [{:card 1} {:card 2} {:card 3} {:card 4}]
                    :helo-track (t/circular-track :values ["High" "High-Even" "Low-Even" "Low" "Low-Odd"
                                           "High-Odd"]
                                  :markers {:black 5})
                    })
      => {:trick [{:card 1} {:card 2} {:card 3} {:card 4}],
          :helo-track #game_rules.components.track.CircularTrack{:values ["High" "High-Even" "Low-Even" "Low" "Low-Odd" "High-Odd"],
                                                                 :markers {:black 0}}}
  (h/adjust-helo-track {:trick [{:card 1} {:card 2} {:card 3} {:card 5}]
                    :helo-track (t/circular-track :values ["High" "High-Even" "Low-Even" "Low" "Low-Odd"
                                           "High-Odd"]
                                  :markers {:black 5})
                    })
      => {:trick [{:card 1} {:card 2} {:card 3} {:card 5}], :helo-track #game_rules.components.track.CircularTrack{:values ["High" "High-Even" "Low-Even" "Low" "Low-Odd" "High-Odd"], :markers {:black 5}}}
      )

(fact "Trick should be empty after clearing"
      (h/clear-trick {:trick [{:card 1} {:card 1} {:card 1} {:card 1} ]}) => {:trick []}
      )

(fact "play card from hand to trick"
  (h/play-a-card {:trick [] :on-turn 0 :players [{:hand [1 2 3 4]}]} 2) =>
    {:trick [{:player 0, :card 3}], :on-turn 0, :players [{:hand [1 2 4]}]}
      )

