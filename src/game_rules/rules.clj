(ns game-rules.rules
  (:require
            [clojure.string :as string]
             [game-rules.state :as s]
             [game-rules.transition :as t]
   )
  )
;; rules should contain a collection of
;; states and transitions

(defn rules [name]
  {:name name :states {}  }
  )

(defn add-state [rules aState]
  (assoc-in rules [:states (keyword (:name aState))] aState)
  )

(defn add-states [rules states]
  (reduce #(add-state %1 %2) rules states)
  )


;; a and be should be state names
(defn add-transition [rules a b conditions command]
  (let [state ((keyword a) rules)
        to (keyword b)]
    (assoc-in rules [:states (keyword a) :transitions to] (t/transition b conditions command))
  ))

(defn transitions-for-state [rules state]
  (get-in rules [:states (keyword state) :transitions])
  )

(defn conditions-met? [game conditions]
  (every? true?
          (map
           #(% game) conditions))
  )

;; todo - add to tests
(conditions-met? {:a 1 :b 2} [ #(= (:a %) 1)  #(= (:b %) 2) ])
(conditions-met? {:a 1 :b 2} [])

;; transition to new state.
;; Check transition requirements.
;; Execute script
;; change state
;; print new state message
(defn transition [game rules transition]
    (if (conditions-met? game (:conditions transition))
      (let [to (:to transition)
            toState (get-in rules [:states (keyword to)])
            g2 (apply (:command transition) [game])
            g3 (assoc-in g2 [:state] to)
        ]
        (println (:message toState))
        g3
        )
      game  ;; conditins not met
      )
  )

;; todo - add to test
;; (def checkers (-> (rules "checkers")
;;     ;;(add-states [(state "setup") (state "player-turn")])
;;     (add-state (s/state "setup"))
;;     (add-state (s/state "player-turn"))
;;     (add-transition "setup" "player-turn")
;;     )
;;   )
;;  (transitions-for-state checkers "setup")
;;  checkers
;;  ;; given rules, a state and a transition - transition to antoher state
;; (s/state "setup")
