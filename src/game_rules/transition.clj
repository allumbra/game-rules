(ns game-rules.transition
  (:require
            [clojure.string :as string])
  )


(defn transition [to conditions command]
  {:to to :conditions conditions :command command}
  )

(defn add-condition [transition condition]
  (conj (:conditions transition) condition)
  )
