(ns game-rules.helo
  (:require
            [clojure.string :as string]
             [game-rules.components.deck :as d]
             [game-rules.components.track :as t]
             [clojure.core.rrb-vector :as fv]
             [game-rules.rules :as r]
             [game-rules.state :as s]
             [game-rules.transition :as tr]
   )
  )

(defn highest-card-strategy [hand trick helo-track]
  (.indexOf hand (apply max hand)))

;; todo move this to a different place
(defn player-agent [play-card-strategy]
  {:play-a-card play-card-strategy }
  )

;; return a player-area
(defn create-player [name color agent]
  {:name name :color color :hand [] :played-card nil :score 0 :agent agent}
  )

(defn player-score-n [player points]
  (assoc-in player [:score] (+ (:score player) points))
  )

(defn score-points-for-player [game player-index points]
  (let [players (:players game)]
  (assoc-in game [:players player-index] (player-score-n (nth players player-index) points))
  )
)


(defn create-players [name-color-list]
  (into [] (map (fn [[n c]] (create-player n c)) name-color-list))
  )

(def components
   {:deck (d/deck (into [] (range 1 41)))
    :helo-track (t/circular-track :values ["High" "High-Even" "Low-Even" "Low" "Low-Odd"
                                           "High-Odd"]
                                  :markers {:black 0})
    :player-area {:hand [] :played-card nil :score 0 }
    :players []
    :state "start"
    :on-turn 0
    :trick []
    }
  )


;; TODO make this more elegant
(defn create-helo-game [players]
  (let [player-area (:player-area components)]
    (loop [i 0 game components]
      (if (< i (count players))
        (let [p (conj player-area (nth players i))
              g (assoc-in game [:players i] p) ]
          (recur (inc i) g)
          )
        game
      )
    )
  )
)


;; commands
(defn setup [game]
  ;; choose first player
  (assoc-in game [:on-turn] (rand-int (count (:players game))))
)

(defn deal-cards [game]
  (let [cards (d/shuffle-deck (:deck game))
        hands (d/draw-hands cards 4 10)
        ]
    (loop [i 0 [cc gg] [hands game]]
      (if (< i 4)
        (let [g (assoc-in gg [:players i :hand] (nth hands i))]
          (recur (inc i) [hands g])
          )
        gg
        )
    )
  ))
(defn advance-to-next-player [game]
  (let [value (mod (inc (:on-turn game)) (count (:players game)))]
      (assoc-in game [:on-turn] value)
  )
)

;; assuming the :trick has a format of :player 0 :card 4
;; 3 scores +2, +1, -1
;; modify :on-turn
(defn score [game]
  (let [helo-index (get-in game [:helo-track :markers :black])
        helo (get-in game [:helo-track :values helo-index])
        is-high? (.contains helo "High")
        is-even? (.contains helo "Even")
        is-odd?  (.contains helo "Odd")
        filtered (cond
                  is-even? (filterv #(= (mod (:card %) 2) 0) (:trick game))
                  is-odd?  (filterv #(not= (mod (:card %) 2) 0) (:trick game))
                  :else (:trick game))
        s (sort-by :card filtered)
        sorted (if is-high? (reverse s) s)

        score-winner (if (>= (count sorted) 1)
                       (score-points-for-player game (:player (first sorted)) 2)
                       game)
        score-2nd    (if (>= (count sorted) 2)
                       (score-points-for-player score-winner (:player (second sorted)) 1)
                       score-winner)
        score-loser  (if (>= (count sorted) 3)
                       (score-points-for-player score-2nd (:player (last sorted)) -1)
                       score-2nd)
        mod-on-turn  (assoc-in score-loser [:on-turn] (mod (dec (:player (first sorted))) (count (:players game)) ) )
        ]
    mod-on-turn
  )
)



;; if evens and odds in trick are tied, advance
;; todo - come up with alternatives for other numbers of players
(defn adjust-helo-track [game]
    (let [evens (count (filterv #(= (mod (:card %) 2) 0) (:trick game)))
          odds (count (filterv #(not= (mod (:card %) 2) 0) (:trick game)))
          option-count (count (get-in game [:helo-track :values]))
          option-value (get-in game [:helo-track :markers :black])]
      (if (= evens odds)
        (assoc-in game [:helo-track :markers :black] (mod (inc option-value) option-count))
        game
      )
    )
  )


(defn clear-trick [game]
  (assoc-in game [:trick] [])
  )

;; score hand
;; determine if we advance the helo track
;; player who won goes last
;; clear trick
(defn end-trick [game]
  (->
    (score game)
    (adjust-helo-track )
    (clear-trick )
   )
  )

;; move a card from player's (on turn) hand to trick
(defn play-a-card [game card-index]
    (let [current-player (:on-turn game)
          card (get-in game [:players current-player :hand card-index])
          add-to-trick (assoc-in game [:trick (count (:trick game))] {:player current-player :card card})
          hand (get-in game [:players current-player :hand])
          remaining-hand (fv/catvec
                          (fv/subvec hand 0 card-index)
                          (fv/subvec hand (inc card-index) (count hand)))
          ]
        (assoc-in add-to-trick [:players current-player :hand] remaining-hand)
      )
  )

;; (defn random-player [game]
;;   (let [state (:state game)]
;;   ;;(cond
;;   ;;  (= state "player-turn")
;;   ;; )
;;     game
;;   )
;;)

;; what do we need to do here?
(defn end-round [])

;; display winner
(defn end-game [])

;; conditiions
(defn end-of-trick? [game]
  (= (count (:trick game)) (count (:players game)))
  )
(defn not-end-of-trick? [game]
  (not (end-of-trick? game))
 )
;; any score over 20?
(defn game-over? [game]
  (> (max (map #(:score %) (:players game))) 20)
  )
(defn not-game-over? [game]
  (not (game-over? game))
  )


;; todo - only let player agent see their own cards and the cards in the trick.  Maybe list of played cards too.
;; should have "public info" branch of the struct that is easy to pass around.
(defn get-player-move [game]
  (let [player-up (get-in game [:players (get-in game [:on-turn])])
        player-agent (:agent player-up)
        get-card-to-play (:play-a-card player-agent)
        trick (get-in game [:trick])
        hand (:hand player-up)
        helo-value (get-in game [:helo-track :values (t/value-of-marker (get-in game [:helo-track]) "black")])  ;;todo cumbersome - need a function for this
        x (println "get-card-to-play" get-card-to-play hand trick helo-value player-up player-agent)
       index-of-card-to-play (get-card-to-play hand trick helo-value)
        y (println "index" index-of-card-to-play)
    ]
    (play-a-card game index-of-card-to-play)
  )
)

(def rules
  (-> (r/rules "helo")
    (r/add-state
       (-> (s/state "start" "Starting Helo Game")
           (s/add-transition (tr/transition "round" [] setup)))
     )

    (r/add-state (->
                  (s/state "round" "Begin round")
                  (s/add-transition (tr/transition "player-turn" [] deal-cards)))
    )

    (r/add-state (->
                  (s/state "player-turn" "Begin Player Turn")
                  (s/add-transition (tr/transition "turn-end" [] get-player-move)))
                  )

    (r/add-state (->
                  (s/state "turn-end" "Player turn ended")
                  (s/add-transition (tr/transition "player-turn" [not-end-of-trick?] advance-to-next-player))
                  (s/add-transition (tr/transition "trick-end" [end-of-trick?] end-trick))
                  )
                 )
    (r/add-state (->
                  (s/state "trick-end" "End of trick")
                  (s/add-transition (tr/transition "round" [not-game-over?] end-round))
                  (s/add-transition (tr/transition "game-time" [game-over?] end-game))
                  )
                 )
    (r/add-state (s/state "game-end" "Game over"))


    )
  )

rules
