(ns game-rules.state
  (:require
            [clojure.string :as string])
  )

;; convenience methods
(defn state [name message]
  {:name name :transitions [] :message message}
  )

(defn add-data [node data]
  (assoc node :data data)
  )

(defn add-transition [node transition]
  (assoc-in node [:transitions (count (:transitions node))] transition)
  )
