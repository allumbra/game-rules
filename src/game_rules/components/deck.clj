(ns game-rules.components.deck
  (:require
            [clojure.string :as string])
  )


;; a deck is just a vector of cards
(defn deck [cards]
  (into [] cards)
  )

;; return a tuple of the deck (after draw) and add to the collection of drawn cards
(defn draw [deck drawn]
  (if (= (count deck) 0)
    [deck drawn]
    [(pop deck) (conj drawn (peek deck))]
    )
  )

;; return a tuple of the deck and a collection of drawn cards
(defn draw-n [deck n]
  (loop [i n [d h] [deck []]]
    (if (or (zero? i) (< (count deck) 1))
      [d h]
      (recur (dec i) (draw d h))
      )
    )
  )

(defn draw-hands [deck hand-count card-count]
  (second
    (loop [i hand-count [d draw-hands] [deck []]]
      (if (or (zero? i) (< (count deck) 1))
        [d draw-hands]
        (let [[dd a-hand] (draw-n d card-count)]
          (recur (dec i) [dd (conj draw-hands a-hand)])
         )
       )
    )
   )
)
(-> (into [] (range 1 41))
    (draw-hands 4 10)
    )
;; shuffle - defined by core.  Leave it as api?
(defn shuffle-deck [deck]
  (shuffle deck)
  )

;; push a card ontop of the stack
(defn discard [deck card]
  (conj deck card)
  )

