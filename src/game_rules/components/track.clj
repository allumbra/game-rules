(ns game-rules.components.track
  (:require
            [clojure.string :as string])
  )

;; Track - a list of values and
;; a collection of markers set at default locations
;; let's make index 0 reserved as the default location that is "off the track"
(defprotocol Track
  (add-marker [track marker-name value])
  (move-marker-n [track marker-name delta])
  (value-of-marker [track marker])
  )


;"a track is a linear sequence of containers that can hold markers
;   containers should have a value (which could be numerical or string).
;  This basic track definition assumes only 1 marker of each type (name)"

(defrecord LinearTrack [values markers]
  Track
  (add-marker [track marker-name value]
   (assoc-in track [:markers (keyword marker-name)] value) )

  (move-marker-n [track marker delta]
    (let [value (value-of-marker track marker)
          new-value (+ value delta)
          value-count (count (:values track))]
      (cond
         (> 0 new-value) (add-marker track marker 0)
         (>= new-value value-count) (add-marker track marker (count (:values track)))
         :else (add-marker track marker new-value)
       )
    )
  )

  (value-of-marker [track marker]
    "value of marker.  Find location of marker and report container value
     assumes there is only one marker (i.e. finds first)"
      (get-in track [:markers (keyword marker)])
  )

)

(defn linearTrack [& {:keys [values markers]}]
  {:pre [values markers]}
  (LinearTrack. values markers) )


(defrecord CircularTrack [values markers]
  Track
    (add-marker [track marker-name value]
     (assoc-in track [:markers (keyword marker-name)] value) )

    (move-marker-n [track marker delta]
      (let [value (value-of-marker track marker)
            value-count (count (:values track))
            new-value (mod (+ value delta) value-count) ]
        (println value value-count new-value)
        (add-marker track marker new-value)
      )
    )

    (value-of-marker [track marker]
    "value of marker.  Find location of marker and report container value
     assumes there is only one marker (i.e. finds first)"
      (get-in track [:markers (keyword marker)])
  )
  )

(defn circular-track [& {:keys [values markers]}]
  {:pre [values markers]}
  (CircularTrack. values markers) )


