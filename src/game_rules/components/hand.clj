(ns game-rules.components.hand
  (:require
            [clojure.string :as str]

   )
  )

(defn display-hand [card-list]
  (map-indexed (fn [i x] (str i ": " x)) card-list)
  )


(display-hand [10 11 12 13])

(let [yayinput (read-line)]
  (if (= yayinput "1234")
    (println "Correct")
    (println "Wrong")))
