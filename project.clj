(defproject game-rules "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/core.rrb-vector "0.0.11"]]
  :main ^:skip-aot game-rules.core
  :source-paths ["src" "src/game_rules"]
  :test-paths ["test" "test/game_rules"]
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}
             :dev {:dependencies [[midje "1.6.0" :exclusions [org.clojure/clojure]]]
                  :plugins [[lein-midje "3.1.3"]]}})
